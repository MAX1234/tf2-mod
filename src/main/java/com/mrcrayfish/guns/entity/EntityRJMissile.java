package com.mrcrayfish.guns.entity;

import com.mrcrayfish.guns.GunConfig;
import com.mrcrayfish.guns.item.ItemGun;
import com.mrcrayfish.guns.object.Gun;
import com.mrcrayfish.guns.world.ProjectileExplosion;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;

public class EntityRJMissile extends EntityMissile {
    public EntityRJMissile(World worldIn) {
        super(worldIn);
    }

    public EntityRJMissile(World worldIn, EntityLivingBase shooter, ItemGun item, Gun modifiedGun) {
        super(worldIn, shooter, item, modifiedGun);
    }

    @Override
    public void onExpired() {
        double x = this.posX;
        double y = this.posY;
        double z = this.posZ;
        boolean particle = this.world instanceof WorldServer;
        Explosion explosion = new ProjectileExplosion(this, x, y, z, GunConfig.SERVER.missiles.explosionRadius, false, false);
        explosion.doExplosionA();
        explosion.doExplosionB(true);
        explosion.clearAffectedBlockPositions();

        if(particle)
        {
            WorldServer worldServer = (WorldServer) this.world;
            worldServer.spawnParticle(EnumParticleTypes.EXPLOSION_HUGE, true, x, y, z, 0, 0.0, 0.0, 0.0, 0);
        }
    }
}
