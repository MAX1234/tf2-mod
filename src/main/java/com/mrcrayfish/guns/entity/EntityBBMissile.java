package com.mrcrayfish.guns.entity;

import com.mrcrayfish.guns.item.ItemGun;
import com.mrcrayfish.guns.object.Gun;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.world.World;

public class EntityBBMissile extends EntityMissile {
    public EntityBBMissile(World worldIn) {
        super(worldIn);
    }

    public EntityBBMissile(World worldIn, EntityLivingBase shooter, ItemGun item, Gun modifiedGun) {
        super(worldIn, shooter, item, modifiedGun);
    }
}
