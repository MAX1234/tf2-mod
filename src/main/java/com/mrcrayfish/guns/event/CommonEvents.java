package com.mrcrayfish.guns.event;

import com.mrcrayfish.guns.Reference;
import com.mrcrayfish.guns.capability.DisguiseProvider;
import com.mrcrayfish.guns.init.ModSounds;
import com.mrcrayfish.guns.item.ItemButterflyKnife;
import com.mrcrayfish.guns.item.ItemConniversKunai;
import com.mrcrayfish.guns.item.ItemDeadRinger;
import com.mrcrayfish.guns.item.ItemGun;
import com.mrcrayfish.guns.object.Gun;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBlaze;
import net.minecraft.client.renderer.entity.RenderEnderman;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.client.renderer.entity.RenderWither;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.*;
import net.minecraftforge.client.event.RenderLivingEvent;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.EntityEvent;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Author: MrCrayfish
 */
public class CommonEvents
{
    public static final DataParameter<Boolean> AIMING = EntityDataManager.createKey(EntityPlayer.class, DataSerializers.BOOLEAN);
    public static final DataParameter<Boolean> RELOADING = EntityDataManager.createKey(EntityPlayer.class, DataSerializers.BOOLEAN);

    /**
     * A custom implementation of the cooldown tracker in order to provide the best experience for
     * players. On servers, Minecraft's cooldown tracker is sent to the client but the latency creates
     * an awkward experience as the cooldown applies to the item after the packet has traveled to the
     * server then back to the client. To fix this and still apply security, we just handle the
     * cooldown tracker quietly and not send cooldown packet back to client. The cooldown is still
     * applied on the client in {@link ItemGun#onItemRightClick} and {@link ItemGun#onUsingTick}.
     */
    private static final Map<UUID, CooldownTracker> COOLDOWN_TRACKER_MAP = new HashMap<>();

    public static CooldownTracker getCooldownTracker(UUID uuid)
    {
        if(!COOLDOWN_TRACKER_MAP.containsKey(uuid))
        {
            COOLDOWN_TRACKER_MAP.put(uuid, new CooldownTracker());
        }
        return COOLDOWN_TRACKER_MAP.get(uuid);
    }

    private Map<UUID, ReloadTracker> reloadTrackerMap = new HashMap<>();

    @SubscribeEvent
    public void onPlayerInit(EntityEvent.EntityConstructing event)
    {
        if(event.getEntity() instanceof EntityPlayer)
        {
            event.getEntity().getDataManager().register(AIMING, false);
            event.getEntity().getDataManager().register(RELOADING, false);
        }
    }

    private EntityLivingBase ent;

    public static final ResourceLocation DISGUISE_CAP = new ResourceLocation(Reference.MOD_ID, "disguise");

    @SubscribeEvent
    public void attachCapability(AttachCapabilitiesEvent<Entity> event)
    {
        if (!(event.getObject() instanceof EntityPlayer)) return;

        event.addCapability(DISGUISE_CAP, new DisguiseProvider());
    }

    @SubscribeEvent
    @SuppressWarnings("unchecked")
    public void onPlayerRender(RenderLivingEvent.Pre<EntityPlayer> event) {
        if (!(event.getEntity() instanceof EntityPlayer))
            return;

        EntityPlayer player = (EntityPlayer) event.getEntity();
        if (player.isSpectator()) {
            return;
        }

        ent = (EntityLivingBase) player.getCapability(DisguiseProvider.DISGUISE_CAP, null).getDisguise();

        if (ent == null) return;

        event.setCanceled(true);

        RenderLivingBase renderer = event.getRenderer();
        renderer = (RenderLivingBase) renderer.getRenderManager().entityRenderMap.get(ent.getClass());

        for (Field field : EntityLivingBase.class.getDeclaredFields()) {
            if ((field.getModifiers() & (Modifier.FINAL | Modifier.STATIC)) != 0) {
                continue;
            }

            field.setAccessible(true);
            try {
                field.set(ent, field.get(player));
            } catch (IllegalAccessException e) {
//                e.printStackTrace();
            }
        }

        renderer.doRender(ent, event.getX(), event.getY(), event.getZ(), 0, event.getPartialRenderTick());
    }

    @SubscribeEvent
    public void livingDamage(LivingDamageEvent event) {
        EntityLivingBase living = event.getEntityLiving();
        for (EnumHand hand : EnumHand.values()) {
            ItemStack held = living.getHeldItem(hand);
            if (held.getItem() instanceof ItemDeadRinger) {
                // Entity has ringer and takes damage
                if (((ItemDeadRinger) held.getItem()).doCloak(living, held, event)) {
                    event.setCanceled(true);
                }
                return;
            }
        }

        for (EnumHand hand : EnumHand.values()) {
            if (living.getHeldItem(hand).getItem() instanceof ItemButterflyKnife || living.getHeldItem(hand).getItem() instanceof ItemDeadRinger) {
                event.setAmount(event.getAmount() * 2f);
            }
        }
    }

    @SubscribeEvent
    public void onPlayerTick(TickEvent.PlayerTickEvent event)
    {
        if(event.phase == TickEvent.Phase.START && !event.player.world.isRemote)
        {
            for (EnumHand hand : EnumHand.values()) {
                ItemStack item = event.player.getHeldItem(hand);
                if (item.getItem() instanceof ItemDeadRinger) {
                    item.setItemDamage(item.getItemDamage() - 1);
                }
            }


            EntityPlayer player = event.player;
            if(player.getDataManager().get(RELOADING))
            {
                if(!reloadTrackerMap.containsKey(player.getUniqueID()))
                {
                    if(!(player.inventory.getCurrentItem().getItem() instanceof ItemGun))
                    {
                        player.getDataManager().set(RELOADING, false);
                        return;
                    }
                    reloadTrackerMap.put(player.getUniqueID(), new ReloadTracker(player));
                }
                ReloadTracker tracker = reloadTrackerMap.get(player.getUniqueID());
                if(!tracker.isSameWeapon(player) || tracker.isWeaponFull() || !tracker.hasAmmo(player))
                {
                    reloadTrackerMap.remove(player.getUniqueID());
                    player.getDataManager().set(RELOADING, false);
                    return;
                }
                if(tracker.canReload(player))
                {
                    tracker.increaseAmmo(player);
                    if(tracker.isWeaponFull() || !tracker.hasAmmo(player))
                    {
                        reloadTrackerMap.remove(player.getUniqueID());
                        player.getDataManager().set(RELOADING, false);
                    }
                }
            }
            getCooldownTracker(player.getUniqueID()).tick();
        }
    }

    private static class ReloadTracker
    {
        private int startTick;
        private int slot;
        private ItemStack stack;
        private Gun gun;

        private ReloadTracker(EntityPlayer player)
        {
            this.startTick = player.ticksExisted;
            this.slot = player.inventory.currentItem;
            this.stack = player.inventory.getCurrentItem();
            this.gun = ((ItemGun) stack.getItem()).getModifiedGun(stack);
        }

        public boolean isSameWeapon(EntityPlayer player)
        {
            return !stack.isEmpty() && player.inventory.currentItem == slot && player.inventory.getCurrentItem() == stack;
        }

        public boolean isWeaponFull()
        {
            if(!stack.hasTagCompound())
            {
                stack.setTagCompound(new NBTTagCompound());
            }
            NBTTagCompound tag = stack.getTagCompound();
            return tag.getInteger("AmmoCount") >= gun.general.maxAmmo;
        }

        public boolean hasAmmo(EntityPlayer player)
        {
            return ItemGun.findAmmo(player, gun.projectile.item) != null;
        }

        public boolean canReload(EntityPlayer player)
        {
            int deltaTicks = player.ticksExisted - startTick;
            return deltaTicks > 0 && deltaTicks % 10 == 0;
        }

        public void increaseAmmo(EntityPlayer player)
        {
            ItemStack ammo = ItemGun.findAmmo(player, gun.projectile.item);
            if(!ammo.isEmpty())
            {
                int amount = Math.min(ammo.getCount(), gun.general.reloadSpeed);
                NBTTagCompound tag = stack.getTagCompound();
                if(tag != null)
                {
                    amount = Math.min(amount, gun.general.maxAmmo - tag.getInteger("AmmoCount"));
                    tag.setInteger("AmmoCount", tag.getInteger("AmmoCount") + amount);
                }
                ammo.shrink(amount);
            }

            String reloadSound = gun.sounds.getReload(gun);
            SoundEvent event = ModSounds.getSound(reloadSound);
            if(event == null)
            {
                event = SoundEvent.REGISTRY.getObject(new ResourceLocation(reloadSound));
            }
            if(event != null)
            {
                player.world.playSound(null, player.posX, player.posY + 1.0D, player.posZ, event, SoundCategory.PLAYERS, 1.0F, 1.0F);
            }
        }
    }
}
