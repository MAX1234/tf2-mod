package com.mrcrayfish.guns.capability;

import net.minecraft.nbt.NBTBase;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;

public class DisguiseProvider implements ICapabilitySerializable<NBTBase>
{
    @CapabilityInject(IDisguise.class)
    public static final Capability<IDisguise> DISGUISE_CAP = null;

    private IDisguise instance = DISGUISE_CAP.getDefaultInstance();

    @Override
    public boolean hasCapability(Capability<?> capability, EnumFacing facing)
    {
        return capability == DISGUISE_CAP;
    }

    @Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing)
    {
        return capability == DISGUISE_CAP ? DISGUISE_CAP.<T> cast(this.instance) : null;
    }

    @Override
    public NBTBase serializeNBT()
    {
        return DISGUISE_CAP.getStorage().writeNBT(DISGUISE_CAP, this.instance, null);
    }

    @Override
    public void deserializeNBT(NBTBase nbt)
    {
        DISGUISE_CAP.getStorage().readNBT(DISGUISE_CAP, this.instance, null, nbt);
    }
}
