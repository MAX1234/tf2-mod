package com.mrcrayfish.guns.capability;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;

import javax.annotation.Nullable;
import java.lang.reflect.InvocationTargetException;

public class DisguiseStorage implements Capability.IStorage<IDisguise> {
    @Nullable
    @Override
    public NBTBase writeNBT(Capability<IDisguise> capability, IDisguise instance, EnumFacing side) {
        if (instance.getDisguise() == null) return null;
        return new NBTTagString(instance.getDisguise().getClass().getName());
    }

    @Override
    public void readNBT(Capability<IDisguise> capability, IDisguise instance, EnumFacing side, NBTBase nbt) {
        if (nbt == null) return;
        String class_ = ((NBTTagString)nbt).getString();
        try {
            instance.setDisguise((Entity)Class.forName(class_).getConstructor(World.class).newInstance(new Object[]{
                    null
            }));
        } catch (Exception e) {

        }
    }
}
