package com.mrcrayfish.guns.capability;

import net.minecraft.entity.Entity;

public interface IDisguise {
    public Entity getDisguise();
    public void setDisguise(Entity entity);
}
