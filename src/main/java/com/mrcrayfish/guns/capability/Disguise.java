package com.mrcrayfish.guns.capability;

import net.minecraft.entity.Entity;

public class Disguise implements IDisguise {
    private Entity ent;

    @Override
    public Entity getDisguise() {
        return ent;
    }

    @Override
    public void setDisguise(Entity entity) {
        this.ent = entity;
    }
}
