package com.mrcrayfish.guns.item;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemAppleGold;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.ResourceLocation;

public class ItemConniversKunai extends ItemButterflyKnife {
    public ItemConniversKunai(ResourceLocation id) {
        super(id);
    }

    @Override
    protected void backstabHandler(ItemStack stack, EntityPlayer player, Entity entity) {
        super.backstabHandler(stack, player, entity);

        if (entity instanceof EntityLivingBase) {
//            player.setAbsorptionAmount(player.getAbsorptionAmount() + ((EntityLivingBase) entity).getHealth());
            int amt = (int)player.getAbsorptionAmount();
            player.removeActivePotionEffect(Potion.getPotionById(22));
            player.addPotionEffect(new PotionEffect(Potion.getPotionById(22), 30 * 20, amt + (int)((EntityLivingBase) entity).getMaxHealth() - 1));
        }
    }
}
