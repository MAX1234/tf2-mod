package com.mrcrayfish.guns.item;

import com.mrcrayfish.guns.ItemStackUtil;
import com.mrcrayfish.guns.client.gui.GuiWorkbench;
import com.mrcrayfish.guns.init.ModSounds;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.*;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class ItemButterflyKnife extends ItemSword {
    public ItemButterflyKnife(ResourceLocation id)
    {
        super(ToolMaterial.IRON);
        this.setTranslationKey(id.getNamespace() + "." + id.getPath());
        this.setRegistryName(id);
        this.setMaxStackSize(1);
    }

    private double angleBetween(Vec3d a, Vec3d b) {
        a = new Vec3d(a.x, 0, a.z);
        b = new Vec3d(b.x, 0, b.z);
        return 2.0d * Math.atan((a.subtract(b)).length()/(a.add(b)).length());
    }

    @Override
    public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity) {
//        player.world.playSound(player, player.posX, player.posY, player.posZ, ModSounds.getSound("knife_attack"), SoundCategory.PLAYERS, 1, 0);
        if (entity instanceof EntityLiving && Math.abs(angleBetween(player.getForward(), entity.getForward())) < Math.PI / 2) {
            entity.attackEntityFrom(DamageSource.GENERIC, 6 * ((EntityLiving) entity).getMaxHealth());
            player.world.spawnParticle(EnumParticleTypes.CRIT_MAGIC, entity.posX, entity.posY, entity.posZ, 0, 0, 0);
            this.backstabHandler(stack, player, entity);
            System.out.printf("Backstabbing %s\n", entity);
            return true;
        }

        return false;
    }

    protected void backstabHandler(ItemStack stack, EntityPlayer player, Entity entity) {

    }
}
