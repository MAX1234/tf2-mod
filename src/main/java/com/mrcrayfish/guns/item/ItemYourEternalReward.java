package com.mrcrayfish.guns.item;

import com.mrcrayfish.guns.capability.DisguiseProvider;
import com.mrcrayfish.guns.capability.IDisguise;
import com.mrcrayfish.guns.event.CommonEvents;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class ItemYourEternalReward extends ItemButterflyKnife {
    public ItemYourEternalReward(ResourceLocation id) {
        super(id);
    }

    @Override
    protected void backstabHandler(ItemStack stack, EntityPlayer player, Entity entity) {
        super.backstabHandler(stack, player, entity);

        if (entity instanceof EntityLivingBase)
            player.<IDisguise>getCapability(DisguiseProvider.DISGUISE_CAP, null).setDisguise(entity);
    }
}
