package com.mrcrayfish.guns.item;

import com.mrcrayfish.guns.ItemStackUtil;
import com.mrcrayfish.guns.Reference;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.IItemPropertyGetter;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.management.PlayerList;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingDamageEvent;

import javax.annotation.Nonnegative;
import javax.annotation.Nullable;
import java.util.Objects;

public class ItemDeadRinger extends Item {
    public ItemDeadRinger(ResourceLocation id) {
        this.setTranslationKey(id.getNamespace() + "." + id.getPath());
        this.setRegistryName(id);
        this.setMaxStackSize(1);
        this.setMaxDamage(400);

        addPropertyOverride(
                new ResourceLocation(Reference.MOD_ID, "active"),
                (stack, worldIn, entityIn) -> ItemStackUtil.createTagCompound(stack).getBoolean("active") ? 1.0f : 0.0f
        );
    }

    public boolean doCloak(EntityLivingBase living, ItemStack itemStack, LivingDamageEvent event) {
        if (ItemStackUtil.createTagCompound(itemStack).getBoolean("active")) {
            living.addPotionEffect(new PotionEffect(Objects.requireNonNull(Potion.getPotionById(14)), 7 * 20));

            if (living instanceof EntityPlayer) {
                EntityPlayer player = (EntityPlayer) living;

                itemStack.setItemDamage(400);
                ItemStackUtil.createTagCompound(itemStack).setBoolean("active", false);

                if (player instanceof EntityPlayerMP) {
                    MinecraftServer server = ((EntityPlayerMP) player).server;
                    PlayerList list = server.getPlayerList();
                    list.getPlayers().forEach((p) -> {
                        p.sendMessage(event.getSource().getDeathMessage(player));
                    });
                } else if (player instanceof EntityPlayerSP) {
                    if (player.getServer() != null)
                        player.getServer().sendMessage(event.getSource().getDeathMessage(player));
//                    ((EntityPlayerSP) player).sendChatMessage(event.getSource().getDeathMessage(player));
                }

//                player.sendMessage(event.getSource().getDeathMessage(player));

                if (!player.world.getGameRules().getBoolean("keepInventory")) {
                    for (ItemStack stack : player.inventory.mainInventory) {
                        EntityItem ent = new EntityItem(player.world, player.posX, player.posY, player.posZ, stack);
                        ent.setPickupDelay(Integer.MAX_VALUE);
                        player.world.spawnEntity(ent);
                    }

                    for (ItemStack stack : player.inventory.armorInventory) {
                        EntityItem ent = new EntityItem(player.world, player.posX, player.posY, player.posZ, stack);
                        ent.setPickupDelay(Integer.MAX_VALUE);
                        player.world.spawnEntity(ent);
                    }

                    for (ItemStack stack : player.inventory.offHandInventory) {
                        EntityItem ent = new EntityItem(player.world, player.posX, player.posY, player.posZ, stack);
                        ent.setPickupDelay(Integer.MAX_VALUE);
                        player.world.spawnEntity(ent);
                    }
                }
            }
            return true;
        }

        return false;
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
        ItemStack item = playerIn.getHeldItem(handIn);
        NBTTagCompound tagCompound = ItemStackUtil.createTagCompound(item);

        if (item.getItemDamage() != 0) {
            return new ActionResult<>(EnumActionResult.FAIL, item);
        }

        if (!tagCompound.hasKey("active")) {
            tagCompound.setBoolean("active", true);
        }

        tagCompound.setBoolean("active", !tagCompound.getBoolean("active"));

        return new ActionResult<>(EnumActionResult.SUCCESS, item);
    }
}
